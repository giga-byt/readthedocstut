.. tut documentation master file, created by
   sphinx-quickstart on Mon Jan 10 14:12:56 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tut's documentation!
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   source/article.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
